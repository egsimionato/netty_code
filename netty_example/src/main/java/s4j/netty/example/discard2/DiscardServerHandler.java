package s4j.netty.example.discard2;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;

/**
 * Handles a server-side channel.
 * (1) DiscardServerHandler extends ChannelInboundHandlerAdapter,
 *     which is an implementation of ChannelInboundhandler,
 *     that provides various event handler methods that you can override.
 * (2) We override the channelRead() event handler method here.
 *     This method is called with received message, whenever new data
 *     is received from a client. In this example, the type of the received message is ByteBuf.
 * (3) To implement the DISCARD protocol, the handler has to ignore the received message.
 *     ByteBuf is a reverencecounted object which has to be released explicity via the release() method.
 *     It is handler's responsibility to release any reference-counted object passed.
 * (4) exceptionCaught() event handler method is called with a Throwable when an exception was raised by
 *     Netty due to an I/O error or by a handler implementation due to the exception thrown while processing events.
 *     In most cases, the caught exception should be logged and its associated channel should be closed here.
 *     Maybe you want to send a response message with an error code before closing the connection.
 */
public class DiscardServerHandler extends ChannelInboundHandlerAdapter { // (1)

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) { // (2)
        ByteBuf in = (ByteBuf) msg;
        try {
            while (in.isReadable()) { // (5)
                System.out.print((char) in.readByte());
                System.out.flush();
            }
            ctx.write(msg); // (6)
            ctx.flush(); // (7)
            // discard the received data silently.
            // ((ByteBuf) msg).release(); // (3)
        } finally {
            ReferenceCountUtil.release(msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) { // (4)
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }
}
